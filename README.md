# bin2

Various utilities for converting binary files to code files for embedded.

## bin2cs

Converts a binary file to a C# byte array.

```
Converts a binary file into C# code for embedding
Usage:
  bin2cs [OPTION...] BIN_FILE

  -n, --name arg       The name of the variable to assign the binary to.
                       (default: Content)
      --namespace arg  The name of the namespace to place the generated
                       variable and class. (Requires a class name to be specified.
                       (default: )
      --class arg      The name of the class to place the variable (default: )
      --access arg     Specifies the access level for the variable (default: internal)
      --static         Specifies that the variable should be marked as static
  -w, --width arg      Specifies the number of columns per row in the
                       formatted output. (default: 8)
  -h, --help           Prints this help text.
```