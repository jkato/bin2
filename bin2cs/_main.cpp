#include "builder.h"
#include "options.h"

using namespace bin2;

int main(int argc, char* argv[])
{
    const auto opts = program_options::parse(argc, argv);
    if (!opts.success())
        return 1;

    // We printed the help text, so we can just exit.
    if (opts.print_help())
        return 0;

    return builder().build(opts);
}