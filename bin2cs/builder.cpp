#include <cstdio>
#include <iostream>
#include <fstream>
#include "builder.h"

namespace fs = std::filesystem;
using namespace bin2;

std::vector<std::string> builder::access_levels =
{
    "internal",
    "private",
    "protected",
    "public",
};

int builder::build(const program_options& options)
{
    // Ensure that the access level we're using is something valid.
    const auto access_level = to_lower(std::string(options.access()));
    const auto is_valid_level = std::ranges::count(access_levels, options.access()) > 0;
    
    if (!is_valid_level)
    {
        printf("Access level %s is not valid.\n", options.access().data());
        return 1;
    }

    const auto have_namespace = !options.namespace_name().empty();
    const auto have_class = !options.class_name().empty();

    // If we have a namespace name, but no class name then we can't generate the output since
    // C# does not allow for classes outside of namespaces.
    if (have_namespace && !have_class)
    {
        printf("A class name was not provided despite a namespace being specified.\n"
               "If a namespace is provided, a class name must also be provided.\n");
        return 1;
    }

    // Read in the target binary file.
    const auto bin_file = fs::path(options.bin_file());
    if (!fs::exists(bin_file))
    {
        printf("Binary file does not exist %s\n", options.bin_file().data());
        return 1;
    }

    const auto file_data = read_file(bin_file);

    // Based on the size of the input file, we'll dynamically adjust the number of bytes per row
    // in the output unless a value was specified in the command line.
    size_t col_size;
    if (options.width() == 0)
    {
        if (file_data.size() <= 256)
            col_size = 8;
        else
            col_size = 16;
    }
    else
    {
        col_size = static_cast<size_t>(options.width());
    }

    std::stringstream output;
    if (have_namespace)
    {
        output << get_indent() << "namespace " << options.namespace_name() << '\n'
               << get_indent() << '{' << '\n';
        add_indent(4);
    }

    if (have_class)
    {
        output << get_indent() << "public class " << options.class_name() << '\n'
               << get_indent() << '{' << '\n';
        add_indent(4);
    }

    output << get_indent() << access_level
        << (options.is_static() ? " static " : " ")
        << "byte[] "
        << options.name()
        << " { get; } =\n"
        << get_indent() << '{';

    add_indent(4);
    std::stringstream ascii_listing;
    for (size_t idx = 0; idx < file_data.size(); idx++)
    {
        char digits[5];
        if (idx % col_size == 0)
        {
            output << '\n' << get_indent();
        }

        const auto skip_end_space =
            (idx + 1) % col_size == 0 && idx > 0 ||
            file_data.size() - 1 == idx;

        uint8_t cur_byte = file_data[idx];
        sprintf_s(digits, sizeof(digits), "0x%02x", cur_byte);
        output << digits << (skip_end_space ? "," : ", ");

        if (options.include_ascii())
        {
            if (is_printable(cur_byte))
                ascii_listing << static_cast<char>(cur_byte);
            else
                ascii_listing << '.';

        }

        if (skip_end_space && options.include_ascii())
        {
            output << " // " << ascii_listing.str();
            ascii_listing = std::stringstream();
        }
    }
    output << '\n';
    pop_indent();

    output << get_indent() << '}' << ';' << '\n';

    if (have_class)
    {
        pop_indent();
        output << get_indent() << '}' << '\n';
        
    }

    if (have_namespace)
    {
        pop_indent();
        output << get_indent() << '}' << '\n';
    }

    std::cout << output.str() << '\n';
    return 0;
}

bool builder::is_printable(uint8_t c)
{
    return (c >= 0x20 && c < 0x7f) ||
        c == 0x80 ||
        (c >= 0x82 && c <= 0x8c) ||
        c == 0x8e ||
        (c >= 0x91 && c <= 0x9c) ||
        c == 0x9e || c == 0x9f ||
        (c >= 0xa1 && c <= 0xac) ||
        (c >= 0xae && c <= 0xff);
}

std::string builder::to_lower(std::string str)
{
    std::transform(str.begin(), str.end(), str.begin(),
        [](uint8_t c) { return std::tolower(c); });

    return str;
}

void builder::add_indent(uint8_t size)
{
    if (!_indents.empty())
        size += _indents.top().size;

    _indents.push(indent{ .size = size, .text = std::string(size, ' ') });
}

std::string_view builder::get_indent()
{
    if (!_indents.empty())
        return _indents.top().text;

    return "";
}

void builder::pop_indent()
{
    if (_indents.empty())
        return;

    _indents.pop();
}

std::vector<uint8_t> builder::read_file(const std::filesystem::path& path) const
{
    std::ifstream input(path, std::ios::binary);

    std::vector<uint8_t> data;
    data.reserve(static_cast<uint8_t>(fs::file_size(path)));

    while(!input.eof())
    {
        data.emplace_back(input.get());
    }

    return data;
}
