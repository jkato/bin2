#pragma once
#include <cstdint>
#include <filesystem>
#include <stack>
#include <string>
#include <vector>
#include "options.h"

namespace bin2
{
    struct indent
    {
        uint8_t     size;
        std::string text;
    };

    class builder
    {
        static std::vector<std::string> access_levels;

        std::stack<indent> _indents;

    public:
        builder() = default;
        ~builder() = default;
        builder(const builder&) = default;
        builder(builder&&) = default;

        int build(const program_options& options);

        builder& operator=(const builder&) = default;
        builder& operator=(builder&&) = default;

    private:
        static bool is_printable(uint8_t c);
        static std::string to_lower(std::string str);

        void add_indent(uint8_t size);
        std::string_view get_indent();
        void pop_indent();

        [[nodiscard]] std::vector<uint8_t> read_file(const std::filesystem::path& path) const;
    };
}