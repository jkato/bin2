#include <sstream>
#include <utility>
#include <cxxopts.hpp>
#include "options.h"

using namespace bin2;

static cxxopts::Options options = cxxopts::Options("bin2cs", "Converts a binary file into C# code for embedding");
static bool options_configured = false;

void setup_options()
{
    if (options_configured) return;

    options.add_options()
        ("n,name", "The name of the variable to assign the binary to.",
            cxxopts::value<std::string>()->default_value("Content"))
        ("namespace", "The name of the namespace to place the generated variable and class. (Requires a class name to be specified.",
            cxxopts::value<std::string>()->default_value(""))
        ("class", "The name of the class to place the variable",
            cxxopts::value<std::string>()->default_value(""))
        ("access", "Specifies the access level for the variable",
            cxxopts::value<std::string>()->default_value("internal"))
        ("static", "Specifies that the variable should be marked as static")
        ("w,width", "Specifies the number of columns per row in the formatted output. "
            "Setting this to 0 will allow the application to automatically select an appropriate column width.",
            cxxopts::value<uint8_t>()->default_value("0"))
        ("a,ascii", "Indicates that the ASCII listing for the binary should be displayed as a comment along with the binary.")
        ("h,help", "Prints this help text.")
        ("binary_file", "The input binary file.",
            cxxopts::value<std::vector<std::string>>());

    options_configured = true;
}

uint8_t program_options::default_width = 8;

program_options program_options::parse(int argc, char* argv[])
{
    auto opts = program_options();
    setup_options();

    try
    {
        options.parse_positional("binary_file");
        const auto result = options.parse(argc, argv);

        // Print help if it was requested, then exit.
        if (result.count("help"))
        {
            printf("%s\n", options.help().c_str());
            opts._success = true;
            opts._print_help = true;
            return opts;
        }

        // Populate the options object based on the arguments parsed.
        opts.access(result["access"].as<std::string>());
        opts.class_name(result["class"].as<std::string>());
        opts.name(result["name"].as<std::string>());
        opts.namespace_name(result["namespace"].as<std::string>());
        opts.width(result["width"].as<uint8_t>());

        opts._include_ascii = result["ascii"].as<bool>();
        opts._static = result["static"].as<bool>();

        const auto bin_files = result["binary_file"].as<std::vector<std::string>>();
        if (!bin_files.empty())
        {
            opts._bin_file = bin_files.back();
        }
    }
    catch (std::exception& ex)
    {
        printf("Failed to read command line: %s\n", ex.what());
        return opts;
    }

    opts._success = true;
    return opts;
}

void program_options::access(std::string value) { _access = std::move(value); }
std::string_view program_options::access() const { return _access; }

std::string_view program_options::bin_file() const { return _bin_file; }

void program_options::class_name(std::string value) { _class = std::move(value); }
std::string_view program_options::class_name() const { return _class; }

void program_options::name(std::string value) { _name = std::move(value); }
std::string_view program_options::name() const { return _name; }

void program_options::namespace_name(std::string value) { _namespace = std::move(value); }
std::string_view program_options::namespace_name() const { return _namespace; }

bool program_options::include_ascii() const { return _include_ascii; }
bool program_options::is_static() const { return _static; }
bool program_options::success() const { return _success; }
bool program_options::print_help() const { return _print_help; }

void program_options::width(uint8_t value) { _width = value; }
uint8_t program_options::width() const { return _width; }

std::string program_options::dump() const
{
    std::stringstream out;
    out
        << "Access:      " << _access << "\n"
        << "Class:       " << _class << "\n"
        << "Name:        " << _name << "\n"
        << "Namespace:   " << _namespace << "\n"
        << "Static:      " << (_static ? "true" : "false") << "\n"
        << "Width:       " << static_cast<int32_t>(_width) << "\n"
        << "\n"
        << "Success:     " << (_success ? "Parse OK" : "Parse Failed") << "\n"
        << "Binary File: " << _bin_file << "\n";

    return out.str();
}
