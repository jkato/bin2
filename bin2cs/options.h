#pragma once
#include <cstdint>
#include <string>

namespace bin2
{
    class program_options
    {
        std::string _access = "internal";
        std::string _class = "";
        std::string _name = "Content";
        std::string _namespace = "";
        uint8_t _width = 8;

        bool _include_ascii = false;
        bool _print_help = false;
        bool _static = false;
        bool _success = false;

        std::string _bin_file;

    public:
        static uint8_t default_width;
        static program_options parse(int argc, char* argv[]);

        ~program_options() = default;
        program_options(const program_options&) = default;
        program_options(program_options&&) = default;

        void access(std::string value);
        [[nodiscard]] std::string_view access() const;

        [[nodiscard]] std::string_view bin_file() const;

        void class_name(std::string value);
        [[nodiscard]] std::string_view class_name() const;

        void name(std::string value);
        [[nodiscard]] std::string_view name() const;

        void namespace_name(std::string value);
        [[nodiscard]] std::string_view namespace_name() const;

        [[nodiscard]] bool include_ascii() const;
        [[nodiscard]] bool is_static() const;
        [[nodiscard]] bool success() const;
        [[nodiscard]] bool print_help() const;

        void width(uint8_t value);
        [[nodiscard]] uint8_t width() const;

        [[nodiscard]] std::string dump() const;

        program_options& operator=(const program_options&) = default;
        program_options& operator=(program_options&&) = default;

    protected:
        program_options() = default;
    };
}